import sys
from PyQt4 import QtGui
from src.forms.code.main import main_ui

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    print sys.platform
    f = main_ui(app)
    f.show()
    sys.exit(app.exec_())