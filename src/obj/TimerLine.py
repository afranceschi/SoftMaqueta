from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QObject, pyqtSignal, Qt

from src.obj.fileManager import fileManager
from src.obj.fileTag import fileTag


class TimerLine(QObject):

    dblClicked = pyqtSignal(QtGui.QTreeWidgetItem, QtGui.QTreeWidgetItem)
    contextSignal = pyqtSignal(QtGui.QTreeWidgetItem)

    def __init__(self, lista):

        QObject.__init__(self)

        self.Widget = lista
        self.timeline = QtGui.QTreeWidget(lista)
        self.timeline.setColumnCount(3)
        self.timeline.setColumnWidth(0, 200)
        self.timeline.setColumnWidth(1, 500)
        self.timeline.setHeaderHidden(True)
        self.timeline.setMinimumWidth(lista.width())
        self.timeline.setMinimumHeight(lista.height())
        self.timeline.setDragDropMode(QtGui.QAbstractItemView.InternalMove)
        self.timeline.doubleClicked.connect(self.DblClick)
        self.timeline.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.timeline.customContextMenuRequested.connect(self.contextMenu)

        self.popup_menu = QtGui.QMenu()
        self.create_popup_menu()

        filemanager = fileManager("conf/map.config")
        self.tagConfigLuces = filemanager.getTagByName("luces")
        self.tagConfigMotores = filemanager.getTagByName("motores")

    def contextMenu(self, pos):
        item = self.timeline.itemAt(pos)
        if item:
            self.popup_menu = QtGui.QMenu()
            self.popup_menu.addAction("Desde aqui", lambda: self.btnDesdeAqui(item))
            self.popup_menu.exec_(self.timeline.mapToGlobal(pos))

    def btnDesdeAqui(self, item):
        self.contextSignal.emit(item)

    def create_popup_menu(self, parent=None):
        self.popup_menu = QtGui.QMenu(parent)
        self.popup_menu.addAction("Desde aqui", self.btnDesdeAqui)

    def addItem(self, text):
        item = QtGui.QTreeWidgetItem(self.timeline)
        item.setText(0, text)
        color = QtGui.QColor()
        color.setBlue(255)
        color.setAlpha(64)
        item.setBackgroundColor(0, color)
        item.setBackgroundColor(1, color)
        item.setBackgroundColor(2, color)
        item.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsDragEnabled |
                      Qt.ItemIsDropEnabled | Qt.ItemIsEditable)
        return item

    def addInfo(self, text, detalle="", command="", item=None):
        if not item:
            item = self.currentItem()

        if not item:
            QtGui.QMessageBox.information(self.Widget, "Atencion", "Nada seleccionado",
                                          QtGui.QMessageBox.Ok)
        else:
            j = QtGui.QTreeWidgetItem(item)
            j.setText(0, text)
            j.setText(1, detalle)
            j.setText(2, command)
            if text == "Start":
                color = QtGui.QColor()
                color.setGreen(255)
                color.setAlpha(64)
                j.setBackgroundColor(0, color)
            if text == "Stop":
                color = QtGui.QColor()
                color.setRed(255)
                color.setAlpha(64)
                j.setBackgroundColor(0, color)

            j.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsDragEnabled)
            self.refreshTree()

    def updateInfo(self, text, detalle, command, item=None):
        if not item:
            item = self.currentItem()

        if not item:
            QtGui.QMessageBox.information(self.Widget, "Atencion", "Nada seleccionado",
                                          QtGui.QMessageBox.Ok)
        else:
            j = self.timeline.currentItem()
            j.setText(0, text)
            j.setText(1, detalle)
            j.setText(2, command)
            j.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsDragEnabled)
            self.refreshTree()

    def refreshTree(self):
        self.timeline.expandAll()

    def DblClick(self):
        if self.timeline.currentItem().parent():
            self.dblClicked.emit(self.timeline.currentItem(), self.timeline.currentItem().parent())

    def currentItem(self):
        if self.timeline.currentItem():
            if self.timeline.currentItem().parent():
                return self.timeline.currentItem().parent()
            else:
                return self.timeline.currentItem()
        else:
            return None

    def currentInfo(self):
        if self.timeline.currentItem():
            if self.timeline.currentItem().childCount() == 0:
                return self.timeline.currentItem()
            else:
                return None
        else:
            return None

    def deleteSelected(self):
        item = self.timeline.currentItem()
        if self.timeline.currentItem().parent():
            item.parent().removeChild(item)
        else:
            self.timeline.takeTopLevelItem(self.timeline.indexOfTopLevelItem(item))

    def clear(self):
        self.timeline.clear()

    def getFileTagList(self):
        taglist = list()
        for i in range(self.timeline.topLevelItemCount()):
            tag = fileTag(self.timeline.topLevelItem(i).text(0))
            for f in range(self.timeline.topLevelItem(i).childCount()):
                child = ""
                for z in range(self.timeline.topLevelItem(i).child(f).columnCount()):
                    child += self.timeline.topLevelItem(i).child(f).text(z) + ","
                tag.lista.append(child)
            taglist.append(tag)
        return taglist

    def loadTagList(self, taglist):
        self.clear()
        for i in range(len(taglist)):
            tag = taglist[i]
            item = self.addItem(tag.name)
            for f in range(len(tag.lista)):
                detalle = ""
                data = tag.lista[f].split(',')
                aux = data[2].split(' ')
                if aux[0] == "L":
                    pos = self.tagConfigLuces.search(aux[1])
                    lista = self.tagConfigLuces.lista[pos].split(',')
                    if lista[1] != "":
                        detalle = "Luz " + lista[1] + "(" + lista[0] + ") a " + aux[2] + " en " + \
                                  aux[3] + " ciclos"
                    else:
                        detalle = "Luz " + lista[0] + " a " + aux[2] + " en " + \
                                  aux[3] + " ciclos"
                if aux[0] == "M":
                    pos = self.tagConfigMotores.search(aux[1])
                    lista = self.tagConfigMotores.lista[pos].split(',')
                    if lista[1] != "":
                        detalle = "Motor " + lista[1] + "(" + lista[0] + ") a " + aux[3] + " en " + \
                                  aux[2] + " ciclos"
                    else:
                        detalle = "Motor " + lista[0] + " a " + aux[3] + " en " + \
                                  aux[2] + " ciclos"

                if data[0] != 'Sonido' and data[0]!= 'Video':
                    self.addInfo(data[0], detalle, data[2], item)
                else:
                    self.addInfo(data[0], data[1], data[2], item)

    def getSelected(self):
        lista = self.timeline.selectedItems()
        if len(lista) > 0:
            return lista[0]
        else:
            return None

    def getChildIndex(self, info):
        item = info.parent()
        r = 0
        for i in range(item.childCount()):
            if item.child(i) == info:
                r = i
        return r

    def getItemIndex(self, item):
        r = 0
        for i in range(self.timeline.topLevelItemCount()):
            if self.timeline.topLevelItem(i) == item:
                r = i
        return r
