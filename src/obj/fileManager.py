from src.obj.fileTag import fileTag


class fileManager(object):

    def __init__(self, nfile):
        self.nameFile = nfile

    def getTags(self):
        try:
            lista = list()
            for i in range(self.getTagsCount()):
                tag = self.getTag(i)
                lista.append(tag)
            return lista
        except IOError as e:
            print e
            return None

    def getTag(self, nTag):
        if nTag < self.getTagsCount():
            try:
                arch = open(self.nameFile, 'r')
                tagName = ""
                tagCount = 0
                lines = arch.readlines()
                for i in range(len(lines)):
                    if lines[i][0] == '[':
                        if tagCount == nTag:
                                f = 1
                                while lines[i][f] != ']':
                                    tagName += lines[i][f]
                                    f += 1
                                tag = fileTag(tagName)
                                i += 1
                                while i < len(lines) and lines[i][0] != '[' and lines[i][0] != "":
                                    aux = ""
                                    for z in range(len(lines[i])-1):
                                        aux += lines[i][z]
                                    tag.append(aux)
                                    i += 1
                        tagCount += 1
                arch.close()
                return tag
            except IOError as e:
                print e
                return None
        else:
            return None

    def getTagByName(self, name):
        tagresult = None
        for i in range(self.getTagsCount()):
            tag = self.getTag(i)
            if tag.name == name:
                tagresult = tag
        return tagresult

    def getTagsCount(self):
        try:
            arch = open(self.nameFile, 'r')
            tagCount = 0
            lines = arch.readlines()
            for i in range(len(lines)):
                if lines[i][0] == '[':
                    tagCount += 1
            arch.close()
            return tagCount
        except IOError as e:
            print e
            return None

    def exist(self):
        try:
            arch = open(self.nameFile, 'r')
            arch.close()
            return True
        except IOError:
            return False

    def createFile(self):
        try:
            arch = open(self.nameFile, 'w')
            arch.close()
            return True
        except IOError as e:
            print e
            return False

    def appendFile(self, tag):
        try:
            arch = open(self.nameFile, 'a')
            arch.write("[" + tag.name + "]" + '\n')
            for i in range(len(tag.lista)):
                arch.write(tag.lista[i] + '\n')
            arch.close()
            return True
        except IOError as e:
            print e
            return False

    def saveFile(self, taglist):
        try:
            arch = open(self.nameFile, 'w')
            arch.close()
            for i in range(len(taglist)):
                self.appendFile(taglist[i])
            return True
        except IOError as e:
            print e
            return False