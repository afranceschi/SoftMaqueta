from PyQt4 import QtGui

from PyQt4.phonon import Phonon

from src.forms.ui.video_ui import Ui_video


class video_ui(QtGui.QDialog):
    def __init__(self, point, filePath):
        QtGui.QDialog.__init__(self)

        self.ui = Ui_video()
        self.ui.setupUi(self)

        self.move(point)

        layout = QtGui.QVBoxLayout()
        self.ui.centralWidget.setLayout(layout)

        self.player = Phonon.VideoPlayer(self.ui.centralWidget)
        layout.setMargin(0)
        layout.addWidget(self.player)

        self.mediaSrc = Phonon.MediaSource(filePath)

        self.showFullScreen()

    def exec_(self):
        self.player.play(self.mediaSrc)
        QtGui.QDialog.exec_(self)

    def close(self):
        self.player.stop()
        QtGui.QDialog.close(self)
