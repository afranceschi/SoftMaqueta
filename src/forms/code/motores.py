from PyQt4 import QtGui

import sys

from src.forms.code.config_motores import CONFIG_MOTORES
from src.forms.ui.motores_ui import Ui_Motores
from src.obj.ObjetoMaqueta import ObjetoMaqueta
from src.obj.fileManager import fileManager


class motores_ui(QtGui.QDialog):

    def __init__(self, conection, command=None):
        QtGui.QDialog.__init__(self)

        self.ui = Ui_Motores()
        self.ui.setupUi(self)

        self.SO = sys.platform
        self.BT = conection

        if not command:
            self.cmd = ObjetoMaqueta()
            self.tipo = "Motores"
            self.cmd.valor2 = 1000
        else:
            self.cmd = command
            if self.cmd.valor1 < 0:
                self.cmd.valor1 *= -1

        filemanager = fileManager("conf/map.config")
        tag = filemanager.getTagByName("motores")

        for i in range(len(tag.lista)):
            self.ui.lista.addItem(tag.lista[i])

        self.changeLista()

        self.ui.intencidad.setValue(self.cmd.valor1)
        self.ui.velocidad.setValue(self.cmd.valor2)

        self.ui.label_int_value.setText(str(self.ui.intencidad.value()))
        self.ui.label_vel_value.setText(str(self.ui.velocidad.value()))
        self.ui.intencidad.valueChanged.connect(self.changeIntencidad)
        self.ui.velocidad.valueChanged.connect(self.changeVelocidad)
        self.ui.btn_test.clicked.connect(self.actTest)

        self.ui.lista.currentIndexChanged.connect(self.changeLista)

        self.ui.lista.setCurrentIndex(self.cmd.id)

    def getValue(self):
        return self.cmd

    def accept(self):
        self.genCommand()
        QtGui.QDialog.accept(self)

    def genCommand(self):
        self.cmd.tipo = "Motor"
        aux = self.ui.lista.currentText().split(',')
        self.cmd.id = aux[0]
        self.cmd.name = aux[1]
        if self.ui.lista.currentIndex() == 1:
            self.cmd.valor1 = int(self.ui.intencidad.value())
        else:
            self.cmd.valor1 = int(self.ui.intencidad.value()) * -1
        self.cmd.valor2 = int(self.ui.velocidad.value())

    def changeIntencidad(self):
        self.ui.label_int_value.setText(str(self.ui.intencidad.value()))

    def changeVelocidad(self):
        self.ui.label_vel_value.setText(str(self.ui.velocidad.value()))

    def changeLista(self):
        if self.ui.lista.currentIndex() == 0:
            self.ui.intencidad.setMaximum(CONFIG_MOTORES.MOTOR0_MAX)
        if self.ui.lista.currentIndex() == 1:
            self.ui.intencidad.setMaximum(CONFIG_MOTORES.MOTOR1_MAX)
        if self.ui.lista.currentIndex() == 2:
            self.ui.intencidad.setMaximum(CONFIG_MOTORES.MOTOR2_MAX)
        if self.ui.lista.currentIndex() == 3:
            self.ui.intencidad.setMaximum(CONFIG_MOTORES.MOTOR3_MAX)

    def actTest(self):
        self.genCommand()
        comando = "M " + str(self.cmd.id) + " " + str(self.cmd.valor2) + " " + str(self.cmd.valor1) + " \n"
        self.writeSerial(comando)

    def writeSerial(self, cmd):
        if self.SO == "darwin":
            self.BT.write(cmd.encode())
        else:
            print cmd