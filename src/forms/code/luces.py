from PyQt4 import QtGui

import sys

from src.forms.ui.luces_ui import Ui_Luces
from src.obj.ObjetoMaqueta import ObjetoMaqueta
from src.obj.fileManager import fileManager


class luces_ui(QtGui.QDialog):

    def __init__(self, conection, command=None):
        QtGui.QDialog.__init__(self)

        self.ui = Ui_Luces()
        self.ui.setupUi(self)

        self.SO = sys.platform
        self.BT = conection

        if not command:
            self.cmd = ObjetoMaqueta()
            self.tipo = "Luz"
            self.cmd.valor2 = 10
        else:
            self.cmd = command

        self.ui.intencidad.setValue(self.cmd.valor1)
        self.ui.velocidad.setValue(self.cmd.valor2)

        self.ui.label_int_value.setText(str(self.ui.intencidad.value()))
        self.ui.label_vel_value.setText(str(self.ui.velocidad.value()))
        self.ui.intencidad.valueChanged.connect(self.changeIntencidad)
        self.ui.velocidad.valueChanged.connect(self.changeVelocidad)
        self.ui.btn_test.clicked.connect(self.actTest)

        filemanager = fileManager("conf/map.config")
        tag = filemanager.getTagByName("luces")

        for i in range(len(tag.lista)):
            self.ui.lista.addItem(tag.lista[i])

        self.ui.lista.setCurrentIndex(self.cmd.id)

    def getValue(self):
        return self.cmd

    def accept(self):
        self.genCommand()
        QtGui.QDialog.accept(self)

    def genCommand(self):
        self.cmd.tipo = "Luz"
        aux = self.ui.lista.currentText().split(',')
        self.cmd.id = aux[0]
        self.cmd.name = aux[1]
        self.cmd.valor1 = int(self.ui.intencidad.value())
        self.cmd.valor2 = int(self.ui.velocidad.value())

    def changeIntencidad(self):
        self.ui.label_int_value.setText(str(self.ui.intencidad.value()))

    def changeVelocidad(self):
        self.ui.label_vel_value.setText(str(self.ui.velocidad.value()))

    def actTest(self):
        self.genCommand()
        comando = "L " + str(self.cmd.id) + " " + str(self.cmd.valor1) + " " + str(self.cmd.valor2) + " \n"
        self.writeSerial(comando)

    def writeSerial(self, cmd):
        if self.SO == "darwin":
            self.BT.write(cmd.encode())
        else:
            print cmd