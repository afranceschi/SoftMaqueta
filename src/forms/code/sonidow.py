from PyQt4 import QtGui

#import time
from PyQt4.phonon import Phonon

#import vlc

from src.forms.ui.videow_ui import Ui_video


class sonido_ui(QtGui.QWidget):
    def __init__(self, point, filePath):
        QtGui.QWidget.__init__(self)

        self.ui = Ui_video()
        self.ui.setupUi(self)

        self.move(point)

        layout = QtGui.QVBoxLayout()
        self.ui.centralWidget.setLayout(layout)

        self.player = Phonon.VideoPlayer(self.ui.centralWidget)
        layout.setMargin(0)
        layout.addWidget(self.player)

        self.mediaSrc = Phonon.MediaSource(filePath)

        #self.showFullScreen()
        #self.show()
        self.player.play(self.mediaSrc)

        #instance = vlc.Instance()
        #self.playervlc = instance.media_player_new()
        #media = instance.media_new(filePath.__str__())
        #self.playervlc.set_media(media)
        #self.playervlc.play()

    def show(self):
        QtGui.QWidget.show(self)

    def close(self):
        self.player.stop()
        #self.playervlc.stop()
        QtGui.QDialog.close(self)
