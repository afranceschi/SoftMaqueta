from PyQt4 import QtGui, QtCore

import sys
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QTableWidget

from src.forms.code.config_motores import CONFIG_MOTORES
from src.forms.ui.config_ui import Ui_ConfigWindow
from src.obj.fileManager import fileManager
from src.obj.fileTag import fileTag


class config_ui(QtGui.QDialog):
    def __init__(self, Port):
        QtGui.QDialog.__init__(self)

        self.BT = Port
        self.SO = sys.platform

        self.LUCES = list()
        self.MOTORES = list()
        self.MOTORES_MAXIMOS = list()

        for i in range(32):
            self.LUCES.append(0)

        for i in range(4):
            self.MOTORES.append(0)

        self.MOTORES_MAXIMOS.append(CONFIG_MOTORES.MOTOR0_MAX * -1)
        self.MOTORES_MAXIMOS.append(CONFIG_MOTORES.MOTOR1_MAX)
        self.MOTORES_MAXIMOS.append(CONFIG_MOTORES.MOTOR2_MAX * -1)
        self.MOTORES_MAXIMOS.append(CONFIG_MOTORES.MOTOR3_MAX * -1)

        self.ui = Ui_ConfigWindow()
        self.ui.setupUi(self)

        self.lucesTable = QTableWidget(self.ui.Luces_widget)
        self.lucesTable.setMinimumWidth(self.ui.Luces_widget.width())
        self.lucesTable.setMinimumHeight(self.ui.Luces_widget.height())
        self.lucesTable.setColumnCount(2)
        self.lucesTable.setRowCount(32)
        self.lucesTable.verticalHeader().hide()
        self.lucesTable.setColumnWidth(1, 616)
        self.lucesTable.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)

        item = QtGui.QTableWidgetItem()
        item.setText("Numero")
        self.lucesTable.setHorizontalHeaderItem(0,item)

        item = QtGui.QTableWidgetItem()
        item.setText("Nombre")
        self.lucesTable.setHorizontalHeaderItem(1, item)

        for i in range(32):
            item = QtGui.QTableWidgetItem()
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            item.setText(str(i))
            self.lucesTable.setItem(i, 0, item)

        self.motoresTable = QTableWidget(self.ui.Motores_widget)
        self.motoresTable.setMinimumWidth(self.ui.Motores_widget.width())
        self.motoresTable.setMinimumHeight(self.ui.Motores_widget.height())
        self.motoresTable.setColumnCount(2)
        self.motoresTable.setRowCount(4)
        self.motoresTable.verticalHeader().hide()
        self.motoresTable.setColumnWidth(1, 630)
        self.motoresTable.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)

        item = QtGui.QTableWidgetItem()
        item.setText("Numero")
        self.motoresTable.setHorizontalHeaderItem(0, item)

        item = QtGui.QTableWidgetItem()
        item.setText("Nombre")
        self.motoresTable.setHorizontalHeaderItem(1, item)

        for i in range(4):
            item = QtGui.QTableWidgetItem()
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            item.setText(str(i))
            self.motoresTable.setItem(i, 0, item)

        self.ui.Btn_continuar.clicked.connect(self.continuarAct)
        self.ui.Btn_luces_test.clicked.connect(self.lucesTestAct)
        self.ui.Btn_motores_test.clicked.connect(self.motoresTestAct)

        self.ui.btnUp_Motor.clicked.connect(self.upMotor)
        self.ui.btnUp_Luz.clicked.connect(self.upLuz)

        self.ui.btnDown_Motor.clicked.connect(self.downMotor)
        self.ui.btnDown_Luz.clicked.connect(self.downLuz)

        self.filemanager = fileManager("conf/map.config")
        if not self.filemanager.exist():
            self.crearConfig()

        self.loadData("motores", self.motoresTable)
        self.loadData("luces", self.lucesTable)

    def loadData(self, name, table):
        tag = self.filemanager.getTagByName(name)
        for i in range(len(tag.lista)):
            aux = tag.lista[i].split(',')
            item = QtGui.QTableWidgetItem()
            item.setText(aux[0])
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            table.setItem(i, 0, item)
            item = QtGui.QTableWidgetItem()
            item.setText(aux[1])
            table.setItem(i, 1, item)

    def genTag(self, name, table):
        tag = fileTag(name)
        for i in range(table.rowCount()):
            tag.append(table.item(i, 0).text() + "," + table.item(i, 1).text())
        return tag

    def continuarAct(self):
        taglist = list()
        taglist.append(self.genTag("motores", self.motoresTable))
        taglist.append(self.genTag("luces", self.lucesTable))
        self.filemanager.saveFile(taglist)
        self.accept()

    def lucesTestAct(self):
        print "Test luz"
        if len(self.lucesTable.selectedItems()) != 0:
            luz = self.lucesTable.item(self.lucesTable.currentRow(), 0).text().__str__()
            nluz = int(luz)
            if self.LUCES[nluz] == 255:
                cmd = "L " + luz + " 0 10 S "
                self.LUCES[nluz] = 0
            else:
                cmd = "L " + luz + " 255 10 S "
                self.LUCES[nluz] = 255
            self.writeSerial(cmd)
        else:
            QtGui.QMessageBox.information(self.parentWidget(), "Info", "Debe seleccionar un item.")

    def motoresTestAct(self):
        print "Test motor"
        if len(self.motoresTable.selectedItems()) != 0:
            motor = self.motoresTable.item(self.motoresTable.currentRow(), 0).text().__str__()
            nmotor = int(motor)
            if self.MOTORES[nmotor] == 0:
                self.MOTORES[nmotor] = self.MOTORES_MAXIMOS[nmotor]
            else:
                self.MOTORES[nmotor] = 0
            cmd = "M " + motor + " 1000 " + str(self.MOTORES[nmotor]) + " S "
            self.writeSerial(cmd)
        else:
            QtGui.QMessageBox.information(self.parentWidget(), "Info", "Debe seleccionar un item.")

    def crearConfig(self):
        print "Creando archivo de configuracion"
        self.filemanager.createFile()
        taglist = list()
        tag1 = fileTag("luces")
        for i in range(32):
            tag1.append(str(i) + ",")
        taglist.append(tag1)

        tag2 = fileTag("motores")
        for i in range(4):
            tag2.append(str(i) + ",")
        taglist.append(tag2)

        self.filemanager.saveFile(taglist)

    def upMotor(self):
        self.upTable(self.motoresTable)

    def upLuz(self):
        self.upTable(self.lucesTable)

    def downMotor(self):
        self.downTable(self.motoresTable)

    def downLuz(self):
        self.downTable(self.lucesTable)

    def upTable(self, table):
        if len(table.selectedItems()) != 0:
            fila = table.currentRow()
            columna = table.currentColumn()
            if fila != 0:
                aux1 = table.item(fila, 0).text()
                aux2 = table.item(fila, 1).text()
                table.item(fila, 0).setText(table.item(fila - 1, 0).text())
                table.item(fila, 1).setText(table.item(fila - 1, 1).text())
                table.item(fila - 1, 0).setText(aux1)
                table.item(fila - 1, 1).setText(aux2)

                table.item(fila, columna).setSelected(False)
                item = table.item(fila - 1, columna)
                item.setSelected(True)
                table.setCurrentItem(item)
        else:
            QtGui.QMessageBox.information(self.parentWidget(), "Info", "Debe seleccionar un item.")

    def downTable(self, table):
        if len(table.selectedItems()) != 0:
            fila = table.currentRow()
            columna = table.currentColumn()
            if fila != (table.rowCount()-1):
                aux1 = table.item(fila, 0).text()
                aux2 = table.item(fila, 1).text()
                table.item(fila, 0).setText(table.item(fila + 1, 0).text())
                table.item(fila, 1).setText(table.item(fila + 1, 1).text())
                table.item(fila + 1, 0).setText(aux1)
                table.item(fila + 1, 1).setText(aux2)

                table.item(fila, columna).setSelected(False)
                item = table.item(fila + 1, columna)
                item.setSelected(True)
                table.setCurrentItem(item)
        else:
            QtGui.QMessageBox.information(self.parentWidget(), "Info", "Debe seleccionar un item.")

    def writeSerial(self, cmd):
        cmd += '\n'
        if self.SO == "darwin":
            self.BT.write(cmd.encode())
            print cmd
        else:
            print cmd
