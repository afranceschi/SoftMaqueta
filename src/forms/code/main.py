import os
import subprocess
import tempfile
from PyQt4 import QtGui

import sys
from time import sleep

import serial

from src.forms.code.config import config_ui
from src.forms.code.luces import luces_ui
from src.forms.code.motores import motores_ui
from src.forms.ui.main_ui import Ui_MainWindow
from src.obj.ObjetoMaqueta import ObjetoMaqueta
from src.obj.TimerLine import TimerLine
from src.obj.fileManager import fileManager


class main_ui(QtGui.QMainWindow):

    def __init__(self, app):
        QtGui.QMainWindow.__init__(self)

        self.SO = sys.platform

        if self.SO == "darwin":
            subprocess.call(["alias", "vlc=/Applications/VLC.app/Contents/MacOS/VLC"])
            self.BT = serial.Serial(port='/dev/tty.TEATRO', baudrate=9600)
        else:
            self.BT = None

        self.APP = app

        self.vid = None
        self.mus = None

        self.formLuces = None
        self.formMotores = None

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.cant_motores = 4
        self.cant_luces = 32

        self.timerline = TimerLine(self.ui.lista_comandos)

        self.timerline.dblClicked.connect(self.DobleClick)
        self.timerline.contextSignal.connect(self.ContextMenu)
        self.ui.Btn_setLuz.clicked.connect(self.LucesClick)
        self.ui.Btn_setMot.clicked.connect(self.MotoresClick)
        self.ui.Btn_newEffect.clicked.connect(self.nuevoEfecto)

        self.ui.btn_start.clicked.connect(self.writeStart)
        self.ui.btn_stop.clicked.connect(self.writeStop)

        self.ui.btn_add_video.clicked.connect(self.selectVideo)
        self.ui.btn_add_video_off.clicked.connect(self.videoOff)
        self.ui.btn_add_sonido.clicked.connect(self.selectSonido)
        self.ui.btn_add_sonido_off.clicked.connect(self.sonidoOff)

        self.ui.btn_Delete.clicked.connect(self.deleteClick)

        self.ui.actionNombres.triggered.connect(self.abrirConf)
        self.ui.actionNuevo.triggered.connect(self.newFile)
        self.ui.actionGuardar.triggered.connect(self.saveFile)
        self.ui.actionAbrir.triggered.connect(self.openFile)

        self.ui.actionReset_Luces.triggered.connect(self.resetLuces)
        self.ui.actionReset_Motores.triggered.connect(self.resetMotores)
        self.ui.actionReset_TODO.triggered.connect(self.resetAll)

        self.ui.actionActualizar.triggered.connect(self.updateSoft)

        self.ui.btn_Test_Code.clicked.connect(self.startCode)

    def startCode(self):
        itemSelected = self.timerline.getSelected()

        if not itemSelected:
            item = self.timerline.timeline.topLevelItem(0)
            self.timerline.timeline.setCurrentItem(item)
            itemSelected = self.timerline.getSelected()

        if itemSelected.text(0) == "Stop":
            itemSelected = self.nextLine(itemSelected)

        if itemSelected:
            if not itemSelected.parent():
                itemSelected.setSelected(False)
                indexItem = self.timerline.getItemIndex(itemSelected)
                itemSelected = self.timerline.currentItem().child(0)
                itemSelected.setSelected(True)
            else:
                indexItem = self.timerline.getItemIndex(itemSelected.parent())

            while itemSelected.text(0) != "Stop":
                self.execLine(itemSelected)
                itemSelected = self.nextLine(itemSelected)
                if not itemSelected:
                    indexItem += 1
                    if indexItem < self.timerline.timeline.topLevelItemCount():
                        item = self.timerline.timeline.topLevelItem(indexItem)
                        self.timerline.timeline.setCurrentItem(item)
                        item.setSelected(False)
                        itemSelected = self.timerline.currentItem().child(0)
                        itemSelected.setSelected(True)
                    else:
                        print "Fin de Archivo"
                        break
        else:
            print "Fin de Archivo"

    def nextLine(self, item=QtGui.QTreeWidgetItem):
        index = self.timerline.getChildIndex(item)
        itemCount = item.parent().childCount()
        groupCount = self.timerline.timeline.topLevelItemCount()
        groupIndex = self.timerline.timeline.indexOfTopLevelItem(item.parent())
        item.setSelected(False)
        index += 1
        if index < itemCount:
            item = self.timerline.currentItem().child(index)
            item.setSelected(True)
            return item
        else:
            groupIndex += 1
            if groupIndex < groupCount:
                item = self.timerline.timeline.topLevelItem(groupIndex)
                self.timerline.timeline.setCurrentItem(item)
                item.setSelected(True)
                return item
            else:
                return None

    def execLine(self, item):
        if item.parent():
            if item.text(0) == "Start":
                self.writeSerial("S ")
            else:
                if item.text(0) == "Video":
                    self.execVideo(item.text(1))
                else:
                    if item.text(0) == "Stop Video":
                        self.killVideo()
                    else:
                        if item.text(0) == "Sonido":
                            self.execMusic(item.text(1))
                        else:
                            if item.text(0) == "Stop Sonido":
                                self.killSonido()
                            else:
                                self.writeSerial(item.text(2).__str__())

    def DobleClick(self, dato, parent):
        info = self.timerline.currentInfo()

        if info.text(0) == "Luz":
            data = info.text(2)
            cmd = data.split(" ")
            obj = ObjetoMaqueta()
            obj.tipo = "Luz"
            obj.id = int(cmd[1])
            obj.valor1 = int(cmd[2])
            obj.valor2 = int(cmd[3])
            obj.mod_flag = True

            self.formLuces = luces_ui(self.BT, obj)
            self.formLuces.accepted.connect(self.LucesGetData)
            self.formLuces.exec_()

        if info.text(0) == "Motor":
            data = info.text(2)
            cmd = data.split(" ")
            obj = ObjetoMaqueta()
            obj.tipo = "Motor"
            obj.id = int(cmd[1])
            obj.valor1 = int(cmd[3])
            obj.valor2 = int(cmd[2])
            obj.mod_flag = True

            self.formMotores = motores_ui(self.BT, obj)
            self.formMotores.accepted.connect(self.MotoresGetData)
            self.formMotores.exec_()

        if info.text(0) == "Video":
            if not self.killVideo():
                self.execVideo(info.text(1))

        if info.text(0) == "Sonido":
            if not self.killSonido():
                self.execMusic(info.text(1))

    def ContextMenu(self, item=QtGui.QTreeWidgetItem):

        if not item.parent():
            parentIndex = self.timerline.timeline.indexOfTopLevelItem(item)
            itemIndex = 0
            self.timerline.timeline.topLevelItem(parentIndex).child(itemIndex)
        else:
            parentIndex = self.timerline.timeline.indexOfTopLevelItem(item.parent())
            itemIndex = item.parent().indexOfChild(item)

        currentParentIndex = 0
        currentItemIndex = 0

        parentCount = self.timerline.timeline.topLevelItemCount()

        lucesArray = list()

        for i in range(32):
            lucesArray.append(0)

        motoresArray = list()

        for i in range(4):
            motoresArray.append(0)

        while currentParentIndex < parentCount:
            itemCount = self.timerline.timeline.topLevelItem(currentParentIndex).childCount()
            currentItemIndex = 0
            while currentItemIndex < itemCount:
                if currentParentIndex == parentIndex and currentItemIndex == itemIndex:
                    break
                else:
                    cmd = self.timerline.timeline.topLevelItem(currentParentIndex).child(currentItemIndex).text(0)
                    if cmd == "Luz" or cmd == "Motor":
                        data = self.timerline.timeline.topLevelItem(currentParentIndex).child(currentItemIndex).text(2)
                        dataarray = data.split(' ')
                        if dataarray[0] == 'L':
                            lucesArray[int(dataarray[1])] = int(dataarray[2])
                        else:
                            motoresArray[int(dataarray[1])] = int(dataarray[3])
                currentItemIndex += 1
            if currentParentIndex == parentIndex and currentItemIndex == itemIndex:
                break
            currentParentIndex += 1
        print lucesArray

        for i in range(32):
            self.writeSerial("L " + str(i) + " " + str(lucesArray[i]) + " 1 S ")
            sleep(0.1)

        print motoresArray

        self.writeSerial("M 1 1000 " + str(motoresArray[1]) + " S ")
        self.writeSerial("M 3 1000 " + str(motoresArray[3]) + " S ")
        self.writeSerial("M 2 1000 " + str(motoresArray[2]) + " S ")
        self.writeSerial("M 0 1000 " + str(motoresArray[0]) + " S ")

    def LucesClick(self):
        if self.timerline.currentItem():
            self.formLuces = luces_ui(self.BT)
            self.formLuces.accepted.connect(self.LucesGetData)
            self.formLuces.exec_()
        else:
            QtGui.QMessageBox.information(self, "Info", "Primero seleccione un efecto")

    def MotoresClick(self):
        if self.timerline.currentItem():
            self.formMotores = motores_ui(self.BT)
            self.formMotores.accepted.connect(self.MotoresGetData)
            self.formMotores.exec_()
        else:
            QtGui.QMessageBox.information(self.parentWidget(), "Info", "Primero seleccione un efecto")

    def LucesGetData(self):
        data = self.formLuces.getValue()
        tipo = "Luz"
        if data.name != "":
            detalle = tipo + " " + data.name + "(" + str(data.id) + ") a " + str(data.valor1) + " en " + str(data.valor2) + " ciclos"
        else:
            detalle = tipo + " " + str(data.id) + " a " + str(data.valor1) + " en " + str(data.valor2) + " ciclos"
        comando = "L " + str(data.id) + " " + str(data.valor1) + " " + str(data.valor2) + " "
        if not data.mod_flag:
            self.timerline.addInfo(tipo, detalle, comando)
        else:
            self.timerline.updateInfo(tipo, detalle, comando)

    def MotoresGetData(self):
        data = self.formMotores.getValue()
        tipo = "Motor"
        if data.name != "":
            detalle = tipo + " " + data.name + "(" + str(data.id) + ") a " + str(data.valor1) + " en " + str(
                data.valor2) + " ciclos"
        else:
            detalle = tipo + " " + str(data.id) + " a " + str(data.valor1) + " en " + str(data.valor2) + " ciclos"
        comando = "M " + str(data.id) + " " + str(data.valor2) + " " + str(data.valor1) + " "
        if not data.mod_flag:
            self.timerline.addInfo(tipo, detalle, comando)
        else:
            self.timerline.updateInfo(tipo, detalle, comando)

    def nuevoEfecto(self):
        self.timerline.addItem("Nuevo Efecto")

    def resetLuces(self):
        for i in range(32):
            self.writeSerial("L " + str(i) + " 0 1 S ")
            sleep(0.1)

    def resetMotores(self):
        self.writeSerial("M 1 1000 0 S ")
        self.writeSerial("M 3 1000 0 S ")
        self.writeSerial("M 2 1000 0 S ")
        self.writeSerial("M 0 1000 0 S ")

    def resetAll(self):
        self.resetLuces()
        self.resetMotores()

    def writeStart(self):
        if self.timerline.currentItem():
            self.timerline.addInfo("Start")
        else:
            QtGui.QMessageBox.information(self, "Info", "Primero seleccione un efecto")

    def writeStop(self):
        if self.timerline.currentItem():
            self.timerline.addInfo("Stop")
        else:
            QtGui.QMessageBox.information(self, "Info", "Primero seleccione un efecto")

    def deleteClick(self):
        if self.timerline.currentItem():
            r = QtGui.QMessageBox.information(self, "Atencion", "Esta seguro que desea eliminar?",
                                              QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)

            if r == QtGui.QMessageBox.Ok:
                self.timerline.deleteSelected()
        else:
            QtGui.QMessageBox.information(self, "Info", "Primero seleccione un efecto o comando")

    def selectVideo(self):
        if self.timerline.currentItem():
            ruta_video = QtGui.QFileDialog.getOpenFileName(self.parentWidget(), "Seleccione video",
                                                           "Video files (*.avi *.mp4)")
            self.timerline.addInfo("Video", ruta_video)
        else:
            QtGui.QMessageBox.information(self.parentWidget(), "Info", "Primero seleccione un efecto")

    def selectSonido(self):
        if self.timerline.currentItem():
            ruta_audio = QtGui.QFileDialog.getOpenFileName(self.parentWidget(), "Seleccione audio",
                                                           "Audio files (*.avi *.mp4)")
            self.timerline.addInfo("Sonido", ruta_audio)
        else:
            QtGui.QMessageBox.information(self.parentWidget(), "Info", "Primero seleccione un efecto")

    def videoOff(self):
        if self.timerline.currentItem():
            self.timerline.addInfo("Stop Video")
        else:
            QtGui.QMessageBox.information(self.parentWidget(), "Info", "Primero seleccione un efecto")

    def sonidoOff(self):
        if self.timerline.currentItem():
            self.timerline.addInfo("Stop Sonido")
        else:
            QtGui.QMessageBox.information(self.parentWidget(), "Info", "Primero seleccione un efecto")

    def execMusic(self, filename):
        self.killSonido()

        cmd = "VLC --no-fullscreen --no-interact '" + filename + "'"
        self.mus = subprocess.Popen(cmd.__str__(), stdout=subprocess.PIPE,
                                    shell=True, preexec_fn=os.setsid)
        #self.mus = sonido_ui(self.APP.desktop().screenGeometry(1).center(), filename)

    def execVideo(self, filename):
        self.killVideo()

        cmd = "VLC '" + filename + "'"
        self.vid = subprocess.Popen(cmd.__str__(), stdout=subprocess.PIPE,
                              shell=True, preexec_fn=os.setsid)

        # self.vid = video_ui(self.APP.desktop().screenGeometry(1).center(), filename)

    def killVideo(self):
        if self.vid:
            print "Existe video"
            print self.vid.pid
            cmd = "kill -9 " + str(self.vid.pid)
            subprocess.Popen(cmd.__str__(), stdout=subprocess.PIPE,
                             shell=True, preexec_fn=os.setsid)
            self.vid = None
            return True
        else:
            return False

    def killSonido(self):
        if self.mus:
            print "Existe video"
            cmd = "kill -9 " + str(self.mus.pid)
            subprocess.Popen(cmd.__str__(), stdout=subprocess.PIPE,
                             shell=True, preexec_fn=os.setsid)
            self.mus = None
            return True
        else:
            return False

    def writeSerial(self, cmd):
        cmd += '\n'
        if self.SO == "darwin":
            self.BT.write(cmd.encode())
            print cmd
        else:
            print cmd

    def abrirConf(self):
        conf = config_ui(self.BT)
        conf.exec_()

    def newFile(self):
        self.timerline.clear()

    def saveFile(self):
        arch = QtGui.QFileDialog.getSaveFileName(self)
        #print arch
        taglist = self.timerline.getFileTagList()
        FM = fileManager(arch)
        FM.saveFile(taglist)

    def openFile(self):
        arch = QtGui.QFileDialog.getOpenFileName()
        self.setWindowTitle("Maqueta [" + arch + "]")
        FM = fileManager(arch)
        taglist = FM.getTags()
        self.timerline.loadTagList(taglist)

    def closeEvent(self, event):
        a = QtGui.QMessageBox.question(self.parentWidget(), "Salir", "Guardar Cambios?", QtGui.QMessageBox.Cancel |
                                       QtGui.QMessageBox.No | QtGui.QMessageBox.Ok)

        if a == QtGui.QMessageBox.Cancel:
            event.ignore()
        else:
            if a == QtGui.QMessageBox.Ok:
                self.saveFile()

            self.killSonido()
            self.killVideo()

    def updateSoft(self):
        print "Buscando Actualizacion"
        with tempfile.TemporaryFile() as tempf:
            proc = subprocess.Popen(['git', 'pull', 'origin', 'update'], stdout=tempf)
            proc.wait()
            tempf.seek(0)
            lines = tempf.readlines()
            if lines[0] == "Already up-to-date.\n":
                QtGui.QMessageBox.information(self.parentWidget(), "Info", "No hay actualizaciones nuevas.")
            else:
                QtGui.QMessageBox.information(self.parentWidget(), "Info", "Software actualizado. Se debe reiniciar"
                                                                           "para aplicar los cambios.")