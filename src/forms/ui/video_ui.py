# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'video.ui'
#
# Created: Sun Jun 12 22:45:28 2016
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_video(object):
    def setupUi(self, video):
        video.setObjectName(_fromUtf8("video"))
        video.resize(400, 300)
        self.horizontalLayout = QtGui.QHBoxLayout(video)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.centralWidget = QtGui.QWidget(video)
        self.centralWidget.setObjectName(_fromUtf8("centralWidget"))
        self.horizontalLayout.addWidget(self.centralWidget)

        self.retranslateUi(video)
        QtCore.QMetaObject.connectSlotsByName(video)

    def retranslateUi(self, video):
        video.setWindowTitle(_translate("video", "Dialog", None))

