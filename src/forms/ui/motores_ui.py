# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'motores.ui'
#
# Created: Mon Jun 13 02:43:22 2016
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Motores(object):
    def setupUi(self, Motores):
        Motores.setObjectName(_fromUtf8("Motores"))
        Motores.resize(333, 225)
        self.buttonBox = QtGui.QDialogButtonBox(Motores)
        self.buttonBox.setGeometry(QtCore.QRect(-20, 180, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.btn_test = QtGui.QPushButton(Motores)
        self.btn_test.setGeometry(QtCore.QRect(10, 180, 95, 31))
        self.btn_test.setObjectName(_fromUtf8("btn_test"))
        self.label_7 = QtGui.QLabel(Motores)
        self.label_7.setGeometry(QtCore.QRect(20, 20, 121, 20))
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label_7.setFont(font)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.label_3 = QtGui.QLabel(Motores)
        self.label_3.setGeometry(QtCore.QRect(0, 100, 80, 20))
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.intencidad = QtGui.QSlider(Motores)
        self.intencidad.setGeometry(QtCore.QRect(80, 100, 160, 18))
        self.intencidad.setMaximum(255)
        self.intencidad.setOrientation(QtCore.Qt.Horizontal)
        self.intencidad.setObjectName(_fromUtf8("intencidad"))
        self.label = QtGui.QLabel(Motores)
        self.label.setGeometry(QtCore.QRect(30, 60, 51, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_int_value = QtGui.QLabel(Motores)
        self.label_int_value.setGeometry(QtCore.QRect(250, 100, 65, 21))
        self.label_int_value.setObjectName(_fromUtf8("label_int_value"))
        self.label_4 = QtGui.QLabel(Motores)
        self.label_4.setGeometry(QtCore.QRect(0, 130, 80, 20))
        self.label_4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.lista = QtGui.QComboBox(Motores)
        self.lista.setGeometry(QtCore.QRect(80, 60, 141, 29))
        self.lista.setObjectName(_fromUtf8("lista"))
        self.label_vel_value = QtGui.QLabel(Motores)
        self.label_vel_value.setGeometry(QtCore.QRect(250, 130, 65, 21))
        self.label_vel_value.setObjectName(_fromUtf8("label_vel_value"))
        self.velocidad = QtGui.QSlider(Motores)
        self.velocidad.setGeometry(QtCore.QRect(80, 130, 160, 18))
        self.velocidad.setMinimum(100)
        self.velocidad.setMaximum(5000)
        self.velocidad.setOrientation(QtCore.Qt.Horizontal)
        self.velocidad.setObjectName(_fromUtf8("velocidad"))

        self.retranslateUi(Motores)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Motores.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Motores.reject)
        QtCore.QMetaObject.connectSlotsByName(Motores)
        Motores.setTabOrder(self.lista, self.intencidad)
        Motores.setTabOrder(self.intencidad, self.velocidad)
        Motores.setTabOrder(self.velocidad, self.btn_test)
        Motores.setTabOrder(self.btn_test, self.buttonBox)

    def retranslateUi(self, Motores):
        Motores.setWindowTitle(_translate("Motores", "Motores", None))
        self.btn_test.setText(_translate("Motores", "Test", None))
        self.label_7.setText(_translate("Motores", "Motores:", None))
        self.label_3.setText(_translate("Motores", "Posicion:", None))
        self.label.setText(_translate("Motores", "Motor:", None))
        self.label_int_value.setText(_translate("Motores", "TextLabel", None))
        self.label_4.setText(_translate("Motores", "Velocidad:", None))
        self.label_vel_value.setText(_translate("Motores", "TextLabel", None))

