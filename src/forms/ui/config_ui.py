# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'config.ui'
#
# Created: Fri Jun 17 21:35:57 2016
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ConfigWindow(object):
    def setupUi(self, ConfigWindow):
        ConfigWindow.setObjectName(_fromUtf8("ConfigWindow"))
        ConfigWindow.resize(795, 575)
        self.Btn_luces_test = QtGui.QPushButton(ConfigWindow)
        self.Btn_luces_test.setGeometry(QtCore.QRect(310, 530, 95, 31))
        self.Btn_luces_test.setObjectName(_fromUtf8("Btn_luces_test"))
        self.Btn_motores_test = QtGui.QPushButton(ConfigWindow)
        self.Btn_motores_test.setGeometry(QtCore.QRect(310, 220, 95, 31))
        self.Btn_motores_test.setObjectName(_fromUtf8("Btn_motores_test"))
        self.Luces_widget = QtGui.QWidget(ConfigWindow)
        self.Luces_widget.setGeometry(QtCore.QRect(10, 310, 731, 211))
        self.Luces_widget.setObjectName(_fromUtf8("Luces_widget"))
        self.label_2 = QtGui.QLabel(ConfigWindow)
        self.label_2.setGeometry(QtCore.QRect(10, 270, 111, 31))
        font = QtGui.QFont()
        font.setPointSize(24)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label = QtGui.QLabel(ConfigWindow)
        self.label.setGeometry(QtCore.QRect(10, 20, 141, 31))
        font = QtGui.QFont()
        font.setPointSize(24)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.Motores_widget = QtGui.QWidget(ConfigWindow)
        self.Motores_widget.setGeometry(QtCore.QRect(9, 59, 731, 151))
        self.Motores_widget.setObjectName(_fromUtf8("Motores_widget"))
        self.Btn_continuar = QtGui.QPushButton(ConfigWindow)
        self.Btn_continuar.setGeometry(QtCore.QRect(640, 530, 95, 31))
        self.Btn_continuar.setObjectName(_fromUtf8("Btn_continuar"))
        self.btnUp_Luz = QtGui.QPushButton(ConfigWindow)
        self.btnUp_Luz.setGeometry(QtCore.QRect(750, 360, 41, 21))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.btnUp_Luz.setFont(font)
        self.btnUp_Luz.setObjectName(_fromUtf8("btnUp_Luz"))
        self.btnDown_Luz = QtGui.QPushButton(ConfigWindow)
        self.btnDown_Luz.setGeometry(QtCore.QRect(750, 450, 41, 21))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.btnDown_Luz.setFont(font)
        self.btnDown_Luz.setObjectName(_fromUtf8("btnDown_Luz"))
        self.btnDown_Motor = QtGui.QPushButton(ConfigWindow)
        self.btnDown_Motor.setGeometry(QtCore.QRect(750, 170, 41, 21))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.btnDown_Motor.setFont(font)
        self.btnDown_Motor.setObjectName(_fromUtf8("btnDown_Motor"))
        self.btnUp_Motor = QtGui.QPushButton(ConfigWindow)
        self.btnUp_Motor.setGeometry(QtCore.QRect(750, 80, 41, 21))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.btnUp_Motor.setFont(font)
        self.btnUp_Motor.setObjectName(_fromUtf8("btnUp_Motor"))

        self.retranslateUi(ConfigWindow)
        QtCore.QMetaObject.connectSlotsByName(ConfigWindow)

    def retranslateUi(self, ConfigWindow):
        ConfigWindow.setWindowTitle(_translate("ConfigWindow", "Dialog", None))
        self.Btn_luces_test.setText(_translate("ConfigWindow", "Test", None))
        self.Btn_motores_test.setText(_translate("ConfigWindow", "Test", None))
        self.label_2.setText(_translate("ConfigWindow", "Luces:", None))
        self.label.setText(_translate("ConfigWindow", "Motores:", None))
        self.Btn_continuar.setText(_translate("ConfigWindow", "Continuar >>", None))
        self.btnUp_Luz.setText(_translate("ConfigWindow", "UP", None))
        self.btnDown_Luz.setText(_translate("ConfigWindow", "DOWN", None))
        self.btnDown_Motor.setText(_translate("ConfigWindow", "DOWN", None))
        self.btnUp_Motor.setText(_translate("ConfigWindow", "UP", None))

