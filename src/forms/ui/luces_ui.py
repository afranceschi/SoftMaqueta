# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'luces.ui'
#
# Created: Mon Jun 13 02:43:01 2016
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Luces(object):
    def setupUi(self, Luces):
        Luces.setObjectName(_fromUtf8("Luces"))
        Luces.resize(333, 225)
        self.buttonBox = QtGui.QDialogButtonBox(Luces)
        self.buttonBox.setGeometry(QtCore.QRect(-20, 180, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.btn_test = QtGui.QPushButton(Luces)
        self.btn_test.setGeometry(QtCore.QRect(10, 180, 95, 31))
        self.btn_test.setObjectName(_fromUtf8("btn_test"))
        self.label_7 = QtGui.QLabel(Luces)
        self.label_7.setGeometry(QtCore.QRect(20, 20, 80, 20))
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label_7.setFont(font)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.label_3 = QtGui.QLabel(Luces)
        self.label_3.setGeometry(QtCore.QRect(0, 100, 80, 20))
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.intencidad = QtGui.QSlider(Luces)
        self.intencidad.setGeometry(QtCore.QRect(80, 100, 160, 18))
        self.intencidad.setMaximum(255)
        self.intencidad.setOrientation(QtCore.Qt.Horizontal)
        self.intencidad.setObjectName(_fromUtf8("intencidad"))
        self.label = QtGui.QLabel(Luces)
        self.label.setGeometry(QtCore.QRect(50, 60, 31, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_int_value = QtGui.QLabel(Luces)
        self.label_int_value.setGeometry(QtCore.QRect(250, 100, 65, 21))
        self.label_int_value.setObjectName(_fromUtf8("label_int_value"))
        self.label_4 = QtGui.QLabel(Luces)
        self.label_4.setGeometry(QtCore.QRect(0, 130, 80, 20))
        self.label_4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.lista = QtGui.QComboBox(Luces)
        self.lista.setGeometry(QtCore.QRect(80, 60, 141, 29))
        self.lista.setObjectName(_fromUtf8("lista"))
        self.label_vel_value = QtGui.QLabel(Luces)
        self.label_vel_value.setGeometry(QtCore.QRect(250, 130, 65, 21))
        self.label_vel_value.setObjectName(_fromUtf8("label_vel_value"))
        self.velocidad = QtGui.QSlider(Luces)
        self.velocidad.setGeometry(QtCore.QRect(80, 130, 160, 18))
        self.velocidad.setMinimum(1)
        self.velocidad.setMaximum(255)
        self.velocidad.setOrientation(QtCore.Qt.Horizontal)
        self.velocidad.setObjectName(_fromUtf8("velocidad"))

        self.retranslateUi(Luces)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Luces.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Luces.reject)
        QtCore.QMetaObject.connectSlotsByName(Luces)
        Luces.setTabOrder(self.lista, self.intencidad)
        Luces.setTabOrder(self.intencidad, self.velocidad)
        Luces.setTabOrder(self.velocidad, self.btn_test)
        Luces.setTabOrder(self.btn_test, self.buttonBox)

    def retranslateUi(self, Luces):
        Luces.setWindowTitle(_translate("Luces", "Luces", None))
        self.btn_test.setText(_translate("Luces", "Test", None))
        self.label_7.setText(_translate("Luces", "Luces:", None))
        self.label_3.setText(_translate("Luces", "Intensidad:", None))
        self.label.setText(_translate("Luces", "Luz:", None))
        self.label_int_value.setText(_translate("Luces", "TextLabel", None))
        self.label_4.setText(_translate("Luces", "Velocidad:", None))
        self.label_vel_value.setText(_translate("Luces", "TextLabel", None))

